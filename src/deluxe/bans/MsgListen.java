package deluxe.bans;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import deluxe.bans.backend.BanData;

public class MsgListen implements PluginMessageListener {

	@SuppressWarnings("deprecation")
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if(channel.equalsIgnoreCase("Access")) {
            ByteArrayInputStream bytestream = new ByteArrayInputStream(message);
            DataInputStream datastream = new DataInputStream(bytestream);
            try {
				String info = datastream.readUTF();
				String[] data = info.split("~");
				if(data[0].equalsIgnoreCase("INFO")) {
					for(Player p : Bukkit.getOnlinePlayers()) {
						if(p.hasPermission("staff.alerts")) {
							p.sendMessage("�e�lSTAFF �7- �f" + data[1]);
						}
					}
				} else if(data[0].equalsIgnoreCase("ALERT")) {
					Bukkit.broadcastMessage("�c[Alert]�f " + data[1]);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } else if(channel.equalsIgnoreCase("BanData")) {
            ByteArrayInputStream bytestream = new ByteArrayInputStream(message);
            DataInputStream datastream = new DataInputStream(bytestream);
				try {
					String info = datastream.readUTF();
					BanData.updateDataFromString(info);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
		
	}

}
