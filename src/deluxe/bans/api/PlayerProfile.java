package deluxe.bans.api;

import java.util.UUID;

public class PlayerProfile {

	private String name;
	private String uuid;
	private Boolean banned;
	private Boolean muted;
	private Long bantime;
	private Long mutetime;
	private String banreason;
	private String mutereason;
	private String banby;
	private String muteby;
	
	public PlayerProfile(String name, String uuid, Boolean banned, Boolean muted, Long bantime, Long mutetime, String banreason, String mutereason, String banby, String muteby) {
		this.name = name;
		this.uuid = uuid;
		this.banned = banned;
		this.muted = muted;
		this.bantime = bantime;
		this.mutetime = mutetime;
		this.banreason = banreason;
		this.mutereason = mutereason;
		this.banby = banby;
		this.muteby = muteby;
	}
	
	public String getName() {
		return name;
	}	
	
	public UUID getUUID() {
        return UUID.fromString(uuid.substring(0, 8) + "-" + uuid.substring(8, 12) + "-" + uuid.substring(12, 16) + "-" + uuid.substring(16, 20) + "-" + uuid.substring(20, 32));
	}
	
	public Boolean isBanned() {
		return banned;
	}
	
	public Boolean isMuted() {
		return muted;
	}
	
	public Long getBanTime() {
		return bantime / 1000L;
	}
	
	public Long getMuteTime() {
		return mutetime / 1000L;
	}
	
	public String getBanReason() {
		return banreason;
	}
	
	public String getMuteReason() {
		return mutereason;
	}
	
	public String getBannedBy() {
		return banby;
	}
	
	public String getMutedBy() {
		return muteby;
	}
}
