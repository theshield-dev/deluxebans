package deluxe.bans.api;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class NameFetcher implements Listener {
	
	private static String HTTP_REQUEST = "http://mcuuid.com/api/";
	public static Map<String, String> data = new HashMap<String, String>();
	
	public static String getNameOf(String uuid) throws Exception
	{
		if(data.containsKey(uuid))
		{
			return data.get(uuid.toString());
		} else {
			String request = getRequest(uuid);
			data.put(uuid, request);
			return request;
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void reload() {
		for(Player p : Bukkit.getOnlinePlayers()) { 
			data.put(p.getUniqueId().toString(), p.getName());
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerLoginEvent event)
	{ 
		logPlayer(event.getPlayer());
	}
	
	public static void logPlayer(Player player)
	{
		if(!(data.containsKey(player.getUniqueId().toString())))
		{
			data.put(player.getUniqueId().toString(), player.getName());
		}
	}
	 
	 public static String getRequest(String player)
	 {
        try {
            URL url = new URL(HTTP_REQUEST + player + "/raw");
            InputStream is = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line = br.readLine()) != null)
                return line;
            br.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
	 }
}

