package deluxe.bans.api;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import the.shield.Core;

public class SQLAPI {
	
	private final Plugin plugin;
    private final String user;
    private final String database;
    private final String password;
    private final String port;
    private final String hostname;

    private Connection connection;

    public SQLAPI(Plugin plugin, String hostname, String port, String database, String username, String password) {
        this.plugin = plugin;
        this.hostname = hostname;
        this.port = port;
        this.database = database;
        this.user = username;
        this.password = password;
        this.connection = null;
    }

    
    public Connection openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.hostname + ":" + this.port + "/" + this.database, this.user, this.password);
        } catch (SQLException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not connect to MySQL server! because: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            plugin.getLogger().log(Level.SEVERE, "JDBC Driver not found!");
        }
        return connection;
    }

    
    public boolean checkConnection() {
        return connection != null;
    }

    
    public Connection getConnection() {
        return connection;
    }

    
    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                plugin.getLogger().log(Level.SEVERE, "Error closing the MySQL Connection!");
                e.printStackTrace();
            }
        }
    }

    @Deprecated
    public ResultSet querySQL(String query) {
        Connection c = getFixedConnection();

        Statement s = null;

        try {
            s = c.createStatement();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        ResultSet ret = null;
        try {
            ret = s.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }
  
    public Boolean valueExists(String query, String value) throws SQLException {
    	Statement s = getFixedConnection().createStatement();
    	ResultSet r = s.executeQuery(query);
    	if(!r.next())
    	{
    		return false;
    	}
    	return true;
    }
    
    public Long getLongQuery(String query, String value) throws SQLException {
    	Statement s = getFixedConnection().createStatement();
    	ResultSet r = s.executeQuery(query);
    	if(!r.next())
    	{
    		return (long) -1;
    	}
    	return r.getLong(value);
    }
    
    public Integer getIntegerQuery(String query, String value) throws SQLException {
    	Statement s = getFixedConnection().createStatement();
    	ResultSet r = s.executeQuery(query);
    	if(!r.next())
    	{
    		return -1;
    	}
    	return r.getInt(value);
    }
    
    public String getStringQuery(String query, String value) throws SQLException {
    	Statement s = getFixedConnection().createStatement();
    	ResultSet r = s.executeQuery(query);
    	if(!r.next())
    	{
    		return null;
    	}
    	return r.getString(value);
    }
    
    public String getQuery(String query, String value) throws SQLException {
    	Statement s = getFixedConnection().createStatement();
    	ResultSet r = s.executeQuery(query);
    	if(!r.next())
    	{
    		return null;
    	}
    	return r.getString(value);
    }
    
    public Connection getFixedConnection()
    {
        if (checkConnection()) {
            return getConnection();
        } else {
            return openConnection();
        }
    }
    

    public void executeUpdate(final String update) {
    	Bukkit.getScheduler().runTaskLaterAsynchronously(Core.plugin, new Runnable() 
    	{
    		public void run()
    		{
    	        Connection c = getFixedConnection();
    	        Statement s = null;

    	        try {
    	            s = c.createStatement();
    	            s.executeUpdate(update);
    	        } catch (SQLException e1) {
    	            e1.printStackTrace();
    	        }
    		}
    	}, 1L);
    }

}
