package deluxe.bans;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import deluxe.bans.api.UUIDFetcher;
import deluxe.bans.backend.Backend;
import deluxe.bans.backend.BanData;

public class BanCommand implements Listener, CommandExecutor {

	private static String name = "§b§lDURATION";
	
	public static void setupInventory(Player player, String username, String reason) {
		Inventory i = Bukkit.createInventory(null, 54, name);
		i.addItem(get(Material.STONE, "§7[§c§l10 MINUTES§7]"));
		i.addItem(get(Material.GRASS, "§7[§c§l30 MINUTES§7]"));
		i.addItem(get(Material.BEACON, "§7[§c§l1 HOUR§7]"));
		i.addItem(get(Material.NETHER_BRICK, "§7[§c§l3 HOURS§7]"));
		i.addItem(get(Material.WOOD, "§7[§c§l12 HOURS§7]"));
		i.addItem(get(Material.GLOWSTONE, "§7[§c§l1 DAY§7]"));
		i.addItem(get(Material.WOOL, "§7[§c§l3 DAYS§7]"));
		i.addItem(get(Material.BEDROCK, "§7[§4§lPERMANENT§7]"));
		i.setItem(49, get(Material.NETHER_BRICK,"§c§lYou are banning: §c" + username, "§c§lReason for ban: §c" + reason));
		player.openInventory(i);
	}
	
	@EventHandler
	public void onClick(final InventoryClickEvent event) {
		final Player player = (Player) event.getWhoClicked();
		if(event.getInventory().getName().equalsIgnoreCase(name)) {
			if(event.getCurrentItem() != null) {
				Material m = event.getCurrentItem().getType();
				Long end = (long) 0;
				if(m == Material.STONE) {
					end = (long) 600;
				} else if(m == Material.GRASS) {
					end = (long) (1800);
				} else if(m == Material.BEACON) {
					end = (long) (3600);
				} else if(m == Material.NETHER_BRICK) {
					end = (long) (10800);
				} else if(m == Material.WOOD) {
					end = (long) (43200);
				} else if(m == Material.GLOWSTONE) {
					end = (long) (86400);
				} else if(m == Material.WOOL) {
					end = (long) (259200);
				} else if(m == Material.REDSTONE_BLOCK) {
					end = (long) (604800);
				} else if(m == Material.BEDROCK) {
					end = (long) 0;
				} else if(m == Material.NETHER_BRICK) {
					
				}
				if(end == -1) {
					event.setCancelled(true);
					player.sendMessage("§cIncorrect Duration or No Permission for this duration.");
					return;
				}
				event.setCancelled(true);
				player.closeInventory();
				final String banuser = event.getInventory().getItem(49).getItemMeta().getDisplayName();
				final String banreason = event.getInventory().getItem(49).getItemMeta().getLore().get(0);
				if(Bukkit.getPlayer(banuser) != null) {	
					Bukkit.getPlayer(banuser).kickPlayer("§4UH OH!\n\n§cYou have been banned. Please re-login to TheShield to view your ban reason and ban expiration date.");
				}
				Long l0 = 0L;
				Long l1 = -1L;
				Long i = end;
				if(i != l0 && i != l1) {
					i = (end * 1000);
				}
				final Long banduration = i;
				//BanDuration is the ommited duration in miliseconds, requiring backend to manually add system time millis from the deluxebans class
				//and then convert it to seconds.
				player.sendMessage("§aPlease wait...");
				Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.getPlugin(), new Runnable() {
					public void run() {
						try {
							Backend.ban(UUIDFetcher.getUUIDOf(banuser).toString(), banreason, player.getName(), banduration, banuser);
						} catch (Exception e) {
							player.sendMessage("§cThis resource is currently unavailable. §8[§eFailed to fetch UUID§8]");
						}
					}
				});
			}
		}
	}
	

	public static ItemStack get(Material m, String name, String lore) {
		ItemStack stack = new ItemStack(m);
		ItemMeta stackm = stack.getItemMeta();
		stackm.setDisplayName(name);
		stackm.setLore(Arrays.asList(lore));
		stack.setItemMeta(stackm);
		return stack;
	}
	public static ItemStack get(Material m, String name) {
		ItemStack stack = new ItemStack(m);
		ItemMeta stackm = stack.getItemMeta();
		stackm.setDisplayName(name);
		stack.setItemMeta(stackm);
		return stack;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			final String[] args) {
			if(cmd.getName().equalsIgnoreCase("ban")) {
				if(sender instanceof Player) {
					final Player player = (Player) sender;
					if(!player.hasPermission("staff.ban")) {
						player.sendMessage("§cPermission Denied.");
						return false;
					}
					if(args.length < 2) {
						player.sendMessage("§cNot enough arguements! Usage: /ban <username> <reason>");
						player.sendMessage("§7The duration menu will open up AFTER this command.");
						return false;
					}
					Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.plugin, new Runnable() {
						public void run() {
							String toban = args[0];
						    StringBuilder str = new StringBuilder();
						    for (int i = 1; i < args.length; i++) {
						    	str.append(args[i] + " ");
						    }
						    String msg = str.toString();
						    try {
								BanData data = Backend.getData(UUIDFetcher.getUUIDOf(toban).toString());
								if(data.isBanned()) {
										player.sendMessage("§cThat user is already banned.");
								} else {
									setupInventory(player, toban, msg);
								}
							} catch (Exception e) {
								player.sendMessage("§cThat player has either never logged in or this command is having an error. Double check the username before reporting to a Dev!");
							}
						}
					});
					return false;
				}
				return false;
			}
		return false;
	}

}
