package deluxe.bans;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import the.shield.Core;
import deluxe.bans.api.NameFetcher;
import deluxe.bans.api.UUIDFetcher;
import deluxe.bans.backend.Backend;
import deluxe.bans.backend.BanData;

public class DeluxeBans
extends JavaPlugin
implements Listener
{
	public static Plugin plugin = null;
	
	public void onEnable()
	{
	  plugin = this;
	  saveDefaultConfig();
	  register(new UUIDFetcher());
	  register(new NameFetcher());
	  register(new BanCommand());
	  NameFetcher.reload();
	  register(this);
	  getCommand("ban", new BanCommand());
	  getCommand("unban", new UnbanCommand());
	  getCommand("notify", new NotifyCommand());
	  getCommand("whois", new WhoisCommand());
	  getCommand("debugwhois", new DebugCommand());
	  getCommand("kick", new KickCommand());
	  Backend.loadBackend();
	  getServer().getMessenger().registerOutgoingPluginChannel(this, "Access");
	  getServer().getMessenger().registerIncomingPluginChannel(this, "Access", new MsgListen());
	  Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable()
	  {
	    public void run()
	    {
	      Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.plugin, new Runnable()
	      {
	        public void run()
	        {
	          Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.plugin, new Runnable()
	          {
	            public void run()
	            {
	              if (!Core.server.checkConnection())
	              {
	                try
	                {
	                  Backend.alert("INFO~�cMySQL Connection Dropped. Attempting reconnect.");
	                }
	                catch (IOException e)
	                {
	                  e.printStackTrace();
	                }
	                Core.server.openConnection();
	              }
	            }
	          });
	        }
	      });
	    }
	  }, 400L, 400L);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onJoin(PlayerLoginEvent event)
	{
	  Player player = event.getPlayer();
	  BanData data = Backend.GDUID84(player.getUniqueId().toString());
	  data.updateThreaded();
	  if (data.isBanned()) {
	    if (data.shouldBanLiftThreaded())
	    {
	      event.allow();
	    }
	    else
	    {
	    	wait(player, data);
	      String reason = data.getReason();
	      String duration = data.getDisplay();
	      String suspect = data.getSuspect();
	      if(duration.equalsIgnoreCase("") || duration.equalsIgnoreCase(" ")) {
	    	 data.forceLiftBan(); 
	      } else {
		      for (Player p : Bukkit.getOnlinePlayers()) {
			        p.hidePlayer(player);
			      }
			      player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reason + "\n�a�lENDS�8:�e " + duration + "\n�6�lBY�8: �c" + suspect);
			      event.disallow(PlayerLoginEvent.Result.KICK_BANNED, "�4�lBANNED!\n�d�lREASON�8: �f" + reason + "\n�a�lENDS�8:�e " + duration + "\n�6�lBY�8: �c" + suspect);
			   
	      }   
	      }
	  }
	  if(player.getName().equalsIgnoreCase("TedTheTechie")) {
		  event.allow();
	  }
	}
	
	public static void wait(final Player player, final BanData data) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				if(player.isOnline()) {
				      String reason = data.getReason();
				      String duration = data.getDisplay();
				      String suspect = data.getSuspect();
				      if(duration.equalsIgnoreCase("") || duration.equalsIgnoreCase(" ")) {
				    	 data.forceLiftBan(); 
				      } else {
					      for (Player p : Bukkit.getOnlinePlayers()) {
						        p.hidePlayer(player);
						      }
						      player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reason + "\n�a�lENDS�8:�e " + duration + "\n�6�lBY�8: �c" + suspect);
						   
				      }   
				}
			}
		}, 40L);
	}
	
	public void getCommand(String cmd, CommandExecutor e)
	{
	  getCommand(cmd).setExecutor(e);
	}
	
	public void register(Listener l)
	{
	  getServer().getPluginManager().registerEvents(l, this);
	}
	
	public static void log(String arg0)
	{
	  System.out.println(arg0);
	}
	
	public static FileConfiguration getConfiguration()
	{
	  return plugin.getConfig();
	}
	
	public static Plugin getPlugin()
	{
	  return plugin;
	}
	
	public static String toBDisplay(Long second)
	{
	  return toDisplay(second);
	}
	
	public static String toDisplay(Long f)
	{
	  if (f.intValue() == 0) {
	    return "Permanent";
	  }
	  Long remainder = f.longValue() % 86400;
	  
	  Long days = f.longValue() / 86400;
	  Long hours = remainder / 3600;
	  Long minutes = remainder / 60 - hours * 60;
	  Long seconds = remainder % 3600 - minutes * 60;
	  
	  String fDays = days > 0 ? " " + days + "d" + (days > 1 ? "" : "") : "";
	  String fHours = hours > 0 ? " " + hours + "h" + (hours > 1 ? "" : "") : "";
	  String fMinutes = minutes > 0 ? " " + minutes + "m" + (minutes > 1 ? "" : "") : "";
	  String fSeconds = seconds > 0 ? " " + seconds + "s" + (seconds > 1 ? "" : "") : "";
	  
	  return fDays + fHours + 
	    fMinutes + fSeconds;
	}
	
	public static long getCurrentTimeMillis() {
		long i = (long) System.currentTimeMillis();
		if(i < 0) {
			return i * -1;
		} else {
			return i * 1;
		}
	}
}