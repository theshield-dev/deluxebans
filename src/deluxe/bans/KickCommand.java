package deluxe.bans;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import deluxe.bans.backend.Backend;

public class KickCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cnd, String arg2,
			String[] args) {
		if(cnd.getName().equalsIgnoreCase("kick"))
		{
			if(sender instanceof Player) {
					Player player = (Player) sender;
					if(!player.hasPermission("staff.kick")) {
						player.sendMessage("�cPermission Denied.");
						return false;
					}
					if(args.length < 2) {
						player.sendMessage("�cInvalid arguements. Usage: /kick <player> <reason>"); 
						return false;
					}
					if(Bukkit.getPlayer(args[0]) != null) {
						Player p = Bukkit.getPlayer(args[0]);
					    StringBuilder str = new StringBuilder();
					    for (int i = 1; i < args.length; i++) {
					    	str.append(args[i] + " ");
					    }
					    String msg = str.toString();
						try {
							Backend.alert("INFO~�e" + p.getName() + "�3has been kicked by �e" + player.getName() + "�3.");
							Backend.alert("INFO~�7� Reason for kick: �a" + msg);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					    p.kickPlayer("�b�lKICKED!\n�e�lREASON�8:�r " + msg + "\n�6�lBY�8: �c" + player.getName());
					} else {
						player.sendMessage("�cPlayer not found.");
					}
				return false;
			}
		}
		return false;
	}

}
