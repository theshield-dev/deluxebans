package deluxe.bans.backend;

import java.sql.SQLException;

import org.bukkit.Bukkit;

import deluxe.bans.DeluxeBans;

public class BanData
{
  private Boolean banned;
  private Long end;
  private String reason;
  private String suspect;
  private Boolean updated;
  private String player;
  
  public BanData(String player, String suspect, String reason, Long end, Boolean banned)
  {
    this.banned = banned;
    this.end = end;
    this.updated = Boolean.valueOf(false);
    this.reason = reason;
    this.suspect = suspect;
    this.player = player;
    Backend.data.add(this);
  }
  
  public long getRawEnd() {
	  return end;
  }
  public String getRawReason() {
	  return reason;
  }
  public String getRawSuspect() {
	  return suspect;
  }
  public Boolean getRawUpdated() {
	  return updated;
  }
  public String getRawPlayer() {
	  return player;
  }
  public Boolean getRawBanned() {
	  return banned;
  }
  
  public static void updateDataFromString(String i){
	String[] meta = i.split("~");  
	Boolean banned = Boolean.parseBoolean(meta[0]);
	Long end = Long.parseLong(meta[1]);
	String reason = meta[2];
	String suspect = meta[3];
	String player = meta[4];
	Backend.getData(player).updateprovide(suspect, reason, end, banned);
  }
  
  public String getDataAsString() {
	  
	  String data = "";
	  data = data + banned + "~";
	  data = data + end + "~";
	  data = data + reason + "~";
	  data = data + suspect + "~";
	  data = data + player + "~";
	  
	  return data;
  }
  
  public boolean isBanned()
  {
    if (this.end.intValue() != -1) {
      return true;
    }
    return false;
  }
  
  public void forceLiftBan()
  {
    this.end = Long.valueOf(-1);
    this.banned = Boolean.valueOf(false);
    this.suspect = "None";
    this.reason = "None";
    Backend.update("BANS", "DURATION", "-1", this.player);
    Backend.update("BANS", "SUSPECT", "None", this.player);
    Backend.update("BANS", "REASON", "None", this.player);
  }
  
  public void delete()
  {
    Backend.data.remove(this);
  }
  
  public boolean shouldBanLiftThreaded()
  {
    Long time = Long.valueOf((int)System.currentTimeMillis());

    if(this.end == -1) {
    	return false;
    } else 
    if ((time >= this.end && (this.end.intValue()!= 0) && (this.end.intValue() !=-1) && (this.banned.booleanValue())))
    {
      this.end = Long.valueOf(-1);
      this.banned = Boolean.valueOf(false);
      this.suspect = "None";
      this.reason = "None";
      Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.getPlugin(), new Runnable()
      {
        public void run()
        {
          Backend.unban(BanData.this.player);
        }
      });
      return true;
    }
    return false;
  }
  
  public boolean shouldBanLift()
  {
	    Long time = Long.valueOf((int)System.currentTimeMillis());
	   
	    	
	     if(this.end == -1) {
	    	return false;
	    } else 
	    if ((time >= this.end && (this.end.intValue()!= 0) && (this.end.intValue() !=-1) && (this.banned.booleanValue())))
	    {
      this.end = Long.valueOf(-1);
      this.banned = Boolean.valueOf(false);
      this.suspect = "None";
      this.reason = "None";
      Backend.unban(this.player);
      return true;
    }
    return false;
  }
  
  public String getReason()
  {
    return this.reason;
  }
  
  public String getSuspect()
  {
    return this.suspect;
  }
  
  public String getUniqueId()
  {
    return this.player;
  }
  
  public Boolean isSame(String uid)
  {
    return Boolean.valueOf(this.player.equalsIgnoreCase(uid));
  }
  
  public String getBDisplay()
  {
    if (!shouldBanLift())
    {
      if (this.end.intValue() == 0) {
        return "Permanent";
      }
      if (this.end.intValue() == -1) {
        return "Not Banned";
      }
      return Backend.getBDisplay(this.end);
    }
    return "Pardon Required.";
  }
  
  
  public String getDisplay() {
	  return getBDisplay();
  }

  public boolean isUpdated()
  {
    return this.updated.booleanValue();
  }
  
  public BanData updateprovide(String suspect, String reason, Long end, Boolean banned)
  {
    this.suspect = suspect;
    this.reason = reason;
    Long l0 = 0L;
    Long l1 = -1L;
    if(end == l1 || end == l0) {
    	this.end = end;
    } else {
    	this.end = (end * 1000);
    }
    this.updated = Boolean.valueOf(true);
    this.banned = banned;
    return this;
  }
  
  public BanData updateThreaded()
  {
    Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.getPlugin(), new Runnable()
    {
      public void run()
      {
        BanData.this.update();
      }
    });
    return this;
  }
  
  public BanData update()
  {
    this.updated = Boolean.valueOf(true);
    try
    {
      if (!Backend.api.valueExists("SELECT * FROM `BANS` WHERE `UUID`='" + this.player + "';", "SUSPECT").booleanValue()) {
        this.suspect = "None";
      } else {
        this.suspect = Backend.api.getStringQuery("SELECT * FROM `BANS` WHERE `UUID`='" + this.player + "';", "SUSPECT");
      }
      if (!Backend.api.valueExists("SELECT * FROM `BANS` WHERE `UUID`='" + this.player + "';", "REASON").booleanValue()) {
        this.reason = "None";
      } else {
        this.reason = Backend.api.getStringQuery("SELECT * FROM `BANS` WHERE `UUID`='" + this.player + "';", "REASON");
      }
      Long end = Backend.api.getLongQuery("SELECT * FROM `BANS` WHERE `UUID`='" + this.player + "';", "DURATION");
      this.end = end;
      Long l0 = 0L;
      Long l1 = -1L;
      if ((this.end != l0) && (this.end != l1)) {
        this.end = Long.valueOf(this.end.longValue() * 1000);
      }
      if (end.intValue() != 0) {
        this.banned = Boolean.valueOf(true);
      } else {
        this.banned = Boolean.valueOf(false);
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return this;
  }
}