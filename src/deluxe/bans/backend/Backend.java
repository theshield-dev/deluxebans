package deluxe.bans.backend;

import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.org.apache.commons.io.output.ByteArrayOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import deluxe.bans.DeluxeBans;
import deluxe.bans.api.NameFetcher;
import deluxe.bans.api.SQLAPI;


public class Backend
{
  public static SQLAPI api = null;
  public static List<BanData> data = new ArrayList<BanData>();
  
  public static void loadBackend()
  {
    DeluxeBans.log("Connecting to MySQL Server....");
    FileConfiguration config = DeluxeBans.getConfiguration();
    String hostname = config.getString("MySQL.host");
    String username = config.getString("MySQL.username");
    String password = config.getString("MySQL.password");
    String database = config.getString("MySQL.database");
    String port = config.getString("MySQL.port");
    api = new SQLAPI(DeluxeBans.getPlugin(), hostname, port, database, username, password);
    DeluxeBans.log("Successfully connected to MySQL server.");
    if (api.checkConnection()) {
      api.executeUpdate("CREATE TABLE IF NOT EXISTS BANS (`UUID` varchar(64), `REASON` text, `SUSPECT` text, `DURATION` int);");
    }
  }
  
  public static SQLAPI getSQLAPI()
  {
    return api;
  }
  
  public static String getDisplay(Long end)
  {
    Long t = Long.valueOf((int)(end.intValue() - DeluxeBans.getCurrentTimeMillis()));
    Long f = Long.valueOf(t.intValue() / 1000);
    return DeluxeBans.toDisplay(f);
  }
  
  public static String getBDisplay(Long end)
  {
	  return getDisplay(end);
  }
  
  @SuppressWarnings("deprecation")
public static void ban(String uid, String reason, String suspect, Long duration, String tokick) {
    try {
		ban(uid, reason, suspect, duration);
	} catch (SQLException e) {
		Bukkit.getConsoleSender().sendMessage("Failed to ban " + uid + " for " + reason + " by" + suspect);
	}
      for (Player p : Bukkit.getOnlinePlayers()) {
        if (p.getUniqueId().toString().equalsIgnoreCase(uid)) {
          Player player = p;
          BanData dataa = getData(uid);
          String reasonn = dataa.getReason();
          String durationn = dataa.getDisplay();
          String suspectt = dataa.getSuspect();
          player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reasonn + "\n�a�lENDS�8:�e " + durationn + "\n�6�lBY�8: �c" + suspectt);
        }
      }
      if (Bukkit.getPlayer(tokick) != null) {
        BanData dataa = getData(uid);
        String reasonn = dataa.getReason();
        String durationn = dataa.getDisplay();
        String suspectt = dataa.getSuspect();
        Player player = Bukkit.getPlayer(tokick);
        player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reasonn + "\n�a�lENDS�8:�e " + durationn + "\n�6�lBY�8: �c" + suspectt);
      }
  }
  
  @SuppressWarnings("deprecation")
public static void ban(String uid, String reason, String suspect, Long duration) throws SQLException {
    if (!api.checkConnection()) {
      api.openConnection();
    }
    if(duration == 0 || duration == -1) {
        if (!api.querySQL("SELECT `DURATION` FROM `BANS` WHERE `UUID`='" + uid + "';").next())
        {
          api.executeUpdate("INSERT INTO `BANS` (`UUID`, `REASON`, `SUSPECT`, `DURATION`) VALUES ('" + uid + "', '" + reason + "', '" + suspect + "', '" + duration + "');");
        }
        else
        {
          update("BANS", "REASON", reason, uid);
          update("BANS", "SUSPECT", suspect, uid);
          update("BANS", "DURATION", duration, uid);
        }
        Boolean b = true;
        if(duration == 0) {
        	b = false;
        }
        getData(uid).updateprovide(suspect, reason, duration, b);
    } else {
    	Long math = (DeluxeBans.getCurrentTimeMillis() + duration) / 1000;
        if (!api.querySQL("SELECT `DURATION` FROM `BANS` WHERE `UUID`='" + uid + "';").next())
        {
          api.executeUpdate("INSERT INTO `BANS` (`UUID`, `REASON`, `SUSPECT`, `DURATION`) VALUES ('" + uid + "', '" + reason + "', '" + suspect + "', '" + math + "');");
        }
        else
        {
          update("BANS", "REASON", reason, uid);
          update("BANS", "SUSPECT", suspect, uid);
          update("BANS", "DURATION", math, uid);
        }
        getData(uid).updateprovide(suspect, reason, math, true);
        try {
			Backend.PacketPlayOutSendBanData(getData(uid));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    try
    {
      alert("INFO~�b" + NameFetcher.getNameOf(uid) + "�c has been �b�lbanned.");
      alert("INFO~�7� Banned by: �a" + suspect);
      alert("INFO~�7� Reason for ban:�a " + reason);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    try
    {
      for (Player p : Bukkit.getOnlinePlayers()) {
        if (p.getUniqueId().toString().equalsIgnoreCase(uid))
        {
          Player player = p;
          BanData dataa = getData(uid);
          String reasonn = dataa.getReason();
          String durationn = dataa.getDisplay();
          String suspectt = dataa.getSuspect();
          player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reasonn + "\n�a�lENDS�8:�e " + durationn + "\n�6�lBY�8: �c" + suspectt);
        }
      }
      if (Bukkit.getPlayer(NameFetcher.getNameOf(uid)) != null)
      {
        BanData dataa = getData(uid);
        String reasonn = dataa.getReason();
        String durationn = dataa.getDisplay();
        String suspectt = dataa.getSuspect();
        Player player = Bukkit.getPlayer(NameFetcher.getNameOf(uid));
        player.kickPlayer("�4�lBANNED!\n�d�lREASON�8: �f" + reasonn + "\n�a�lENDS�8:�e " + durationn + "\n�6�lBY�8: �c" + suspectt);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static void unban(String uid)
  {
    getData(uid).forceLiftBan();
    try {
      alert("INFO~�b" + NameFetcher.getNameOf(uid) + "�c has been �b�lunbanned.");
      alert("INFO~�7� Unbanned by: �aSOMEONE, no but seriously, this is broken - alert a Dev of this message.");
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static BanData GDUID84(String GZN1)
  {
    return getData(GZN1);
  }
  
  public static void update(String table, String value, String replace, String key)
  {
    api.executeUpdate("UPDATE `" + table + "` SET `" + value + "`='" + replace + "' WHERE `UUID`='" + key + "';");
  }
  
  public static void update(String table, String value, Long replace, String key)
  {
    api.executeUpdate("UPDATE `" + table + "` SET `" + value + "`='" + replace + "' WHERE `UUID`='" + key + "';");
  }
  
  public static BanData getData(String uuid)
  {
    BanData de = null;
    for (BanData d : data) {
      if (d.isSame(uuid).booleanValue()) {
        de = d;
      }
    }
    if (de == null) {
      return getNewData(uuid);
    }
    return de;
  }
  
  public static BanData getNewData(String uuid)
  {
    return new BanData(uuid, uuid, null, Long.valueOf(-1), Boolean.valueOf(false)).update();
  }
  
  @SuppressWarnings({ "deprecation", "resource" })
public static void alert(String message)
    throws IOException
  {
	  if(Bukkit.getOnlinePlayers().length < 0) {
		  return;
	  }
    DeluxeBans.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(DeluxeBans.getPlugin(), "Access");
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    out.writeUTF(message);
    Bukkit.getOnlinePlayers()[new java.util.Random().nextInt(Bukkit.getOnlinePlayers().length)].sendPluginMessage(DeluxeBans.getPlugin(), "Access", b.toByteArray());
  }

  
  @SuppressWarnings({ "deprecation", "resource" })
public static void PacketPlayOutSendBanData(BanData data) throws IOException {
	  if(Bukkit.getOnlinePlayers().length < 0) {
		  return;
	  }
    DeluxeBans.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(DeluxeBans.getPlugin(), "BanData");
    ByteArrayOutputStream b = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(b);
    out.writeUTF(data.getDataAsString());
    Bukkit.getOnlinePlayers()[new java.util.Random().nextInt(Bukkit.getOnlinePlayers().length)].sendPluginMessage(DeluxeBans.getPlugin(), "BanData", b.toByteArray());
  }
  public static boolean isBanned(String uid)
  {
    return getData(uid).isBanned();
  }
}

