package deluxe.bans.backend;

public enum TimeFormat {

	MINUTE, HOUR, DAY
}
