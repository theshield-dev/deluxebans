package deluxe.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import deluxe.bans.api.UUIDFetcher;
import deluxe.bans.backend.Backend;

public class UnbanCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			final String[] args) {
			if(cmd.getName().equalsIgnoreCase("unban")) {
				if(sender instanceof Player) {
					final Player player = (Player) sender;
					if(!player.hasPermission("staff.unban")) {
						player.sendMessage("�cPermission Denied.");
						return false;
					}
					if(args.length == 0) {
						player.sendMessage("�cNo arguements specified. /unban <username>");
						return false;
					}
					player.sendMessage("�aPlease wait...");
					Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.plugin, new Runnable() {
						public void run() {
							String tounban = args[0];
							if(tounban.length() <= 16) {
								try {
									player.sendMessage("�cAttempting to unban �e" + tounban + "�c. �8[�4Type�8:�f USERNAME�8]");
									Backend.unban(UUIDFetcher.getUUIDOf(tounban).toString());
									Backend.getData(tounban).forceLiftBan();
								} catch (Exception e) {
									player.sendMessage("�cFailed to fetch UUID for �e" + tounban + "�c. �8[�4Type�8:�f USERNAME�8]");
								}
							} else {
								Backend.unban(tounban);
								Backend.getData(tounban).forceLiftBan();
								player.sendMessage("�cAttempting to unban �e" + tounban + "�c. �8[�4Type�8:�f UUID�8]");
							}
						}
					});
				}
			}
			return false;
	}
}
