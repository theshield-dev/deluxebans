package deluxe.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import deluxe.bans.api.UUIDFetcher;
import deluxe.bans.backend.Backend;
import deluxe.bans.backend.BanData;

public class WhoisCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			final String[] args) {
			if(cmd.getName().equalsIgnoreCase("whois")) {
				if(sender instanceof Player) {
					final Player player = (Player) sender;
					if(!player.hasPermission("staff.whois")) {
						player.sendMessage("�cPermission Denied.");
						return false;
					}
					if(args.length == 0) {
						player.sendMessage("�cPlease specify an arguement. /whois <username>");
						return false;
					}
					player.sendMessage("�aPlease wait...");
					Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.plugin, new Runnable() {
						public void run() {
							String tofind = args[0];
							try {
								BanData data = Backend.getData(UUIDFetcher.getUUIDOf(tofind).toString());
								data.update();
								if(data.isBanned() && data.shouldBanLift()) {
									data.forceLiftBan();
									Backend.unban(data.getUniqueId());
								}
								player.sendMessage("�8---------------");
								player.sendMessage("�e�lWHOIS�8: �7" + tofind);
								player.sendMessage("�d�lBANNED�8: �4" + booleanToString(data.isBanned()));
								if(data.isBanned()) {
									player.sendMessage("�5�lREASON�8: �f" + data.getReason());
									player.sendMessage("�b�lSUSPECT�8: �f" + data.getSuspect());
									player.sendMessage("�6�lDURATION�8: �e" + data.getBDisplay());
									player.sendMessage("�8---------------");
									player.sendMessage("�7End of Whois.");
								} else {
									player.sendMessage("�8---------------");
									player.sendMessage("�7End of Whois.");
								}
							} catch (Exception e) {
								player.sendMessage("�cFailed to fetch UUID.");
							}
						}
					});
				}
				return false;
			}
		return false;
	}
	
	public static String booleanToString(Boolean b) {
		if(b) {
			return "�4Yes";
		} else {
			return "�aNo";
		}
	}

}
