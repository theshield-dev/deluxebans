package deluxe.bans;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import deluxe.bans.backend.Backend;

public class NotifyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			String[] args) {
				if(cmd.getName().equalsIgnoreCase("notify")) {
					if(sender instanceof Player) {
						Player player = (Player) sender;
						if(!player.hasPermission("staff.networknotify")) {
							player.sendMessage("�cPermission Denied.");
							return false;
						}
						if(args.length == 0) {
							player.sendMessage("�cPlease specify more arguements. /notify <message>");
							return false;
						}
					    StringBuilder str = new StringBuilder();
					    for (int i = 0; i < args.length; i++) {
					    	str.append(args[i] + " ");
					    }
					    String msg = str.toString();
					    try {
					    	Backend.alert("ALERT~�e" + player.getName() + " �7 - �a" + msg);
						} catch (IOException e) {
							player.sendMessage("�cUnable to send alert.");
						}
					    return false;
					}
					CommandSender player = sender;
					if(args.length == 0) {
						player.sendMessage("�cPlease specify more arguements. /notify <message>");
						return false;
					}
				    StringBuilder str = new StringBuilder();
				    for (int i = 0; i < args.length; i++) {
				    	str.append(args[i] + " ");
				    }
				    String msg = str.toString();
				    try {
						Backend.alert("ALERT~�e" + player.getName() + " �7 - �a" + msg);
					} catch (IOException e) {
						player.sendMessage("�cUnable to send alert.");
					}
				}
		return false;
	}

}
