package deluxe.bans;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import the.shield.U;
import the.shield.core.common.utils.UUIDFetcher;
import deluxe.bans.backend.Backend;
import deluxe.bans.backend.BanData;

public class DebugCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2,
			final String[] args) {
			if(cmd.getName().equalsIgnoreCase("debugwhois")) {
				if(sender instanceof Player) {
					final Player player = (Player) sender;
					if(!player.hasPermission("staff.debugreport")) {
						U.message(player, "�cPermission Denied.");
						return false;
					}
					if(args.length == 0) {
						U.message(player, "�eUsage�8: �f/debugwhois <player>");
						return false;
					}
					Bukkit.getScheduler().runTaskAsynchronously(DeluxeBans.getPlugin(), new Runnable() {
						public void run() {
							try {
								UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
								String uuids = uuid.toString();
								BanData data = Backend.getData(uuids);
								player.sendMessage("�8--------------------");
								player.sendMessage("�eWhois�8: �f" + args[0]);
								player.sendMessage("�cBanned�8: �f" + data.getRawBanned());
								player.sendMessage("�cEnd�8: �f" + data.getRawEnd());
								player.sendMessage("�cPlayer�8: �f" + data.getRawPlayer());
								player.sendMessage("�cReason�8: �f" + data.getRawReason());
								player.sendMessage("�8--------------------");
							} catch (Exception e) {
								U.message(player, "�cFailed to fetch UUID.");
							}
						}
					});
					return false;
				}
				return false;
			}
		return false;
	}

}
